package Calcular;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class mainF extends JFrame implements ActionListener {
	
    ArrayList<String>Question = new ArrayList<String>();//题目存储列表
    ArrayList<String>Answer = new ArrayList<String>();//答案存储列表
    ArrayList<String>answer = new ArrayList<String>();//用户答案存储列表
	int right =0;
	int num = 0;
	private static final long serialVersionUID = 1L;
	private boolean isRun = false;
	private MyRunable myTimeRunable = new MyRunable();
	
	//创建组件对象
	JLabel jLabel0=new JLabel("題目");
	JTextArea JTextArea0=new JTextArea();
	JLabel jLabel1=new JLabel("請輸入答案：");
	JTextArea JTextArea1=new JTextArea();
	JLabel jLabel2=new JLabel("正確答案：");
	JTextArea JTextArea2=new JTextArea();
	JLabel jLabel3=new JLabel("正確率：");
	JTextArea JTextArea3=new JTextArea();
	JButton jButton0=new JButton("確定");
	JButton jButton1=new JButton("繼續做題");
    JButton jButton2=new JButton("計時開始");
	JLabel jLabel4= new JLabel("00:00:00");

	
	//面板初始化
	private void jbInit() throws Exception{
		// TODO Auto-generated method stub
		//框架的布局
		this.setLayout(null);
		//主框架的大小
		setSize(new Dimension(500,450));
		//设置各组件大小
		jLabel0.setFont(new java.awt.Font("楷体",Font.BOLD,15));
		jLabel0.setBounds(new Rectangle(70,2,140,30));
		JTextArea0.setBounds(new Rectangle(30,30,125,230));
		jLabel1.setFont(new java.awt.Font("楷体",Font.BOLD,15));
		jLabel1.setBounds(new Rectangle(195,2,180,30));
		JTextArea1.setBounds(new Rectangle(180,30,125,230));
		jLabel2.setFont(new java.awt.Font("楷体",Font.BOLD,15));
		jLabel2.setBounds(new Rectangle(350,2,180,30));
		JTextArea2.setBounds(new Rectangle(330,30,125,230));
		jLabel3.setFont(new java.awt.Font("楷体",Font.BOLD,18));
		jLabel3.setBounds(new Rectangle(70,280,200,30));
		JTextArea3.setBounds(new Rectangle(170,280,150,30));
		jLabel4.setFont(new java.awt.Font("楷体",Font.BOLD,18));
		jLabel4.setBounds(new Rectangle(360,280,200,30));
		jButton0.setBounds(new Rectangle(190, 340, 100, 30));
		jButton1.setBounds(new Rectangle(340, 340, 100, 30));
		jButton2.setBounds(new Rectangle(40, 340, 100, 30));
		//添加组件到面板
		this.add(jLabel0);
		this.add(JTextArea0);
		this.add(jLabel1);
		this.add(JTextArea1);
		this.add(jLabel2);
		this.add(JTextArea2);
		this.add(jLabel3);
		this.add(JTextArea3);
		this.add(jLabel4);
		this.add(jButton0);
		this.add(jButton1);
		this.add(jButton2);
		//添加事件监听器
		jButton0.addActionListener(this);
		jButton1.addActionListener(this);
		jButton2.addActionListener(this);
	}
	
	
	public static String createAc(int operator_no) {
	    // 表达式
	    String ac = "";
	    String[] operator=new String[]{"+","-","*","÷"};
	    Random rand = new Random();
	    // 括号标记数
	    int bracket = 0;
	    // 括号对数
	    int bracket_no = 0;
	    for (int i = 1; i <= operator_no + 1; i++) {
	        // 判断是否存在"("，若存在，判断是否加上")",bracket=0时不加
	        if (bracket_no != 0 && (bracket = rand.nextInt(3)) != 0
	                && ac.charAt(ac.length() - 1) != '(') {
	            ac = ac + createNum() + ")" + operator[rand.nextInt(4)];
	            bracket_no--;
	        }
	        // 最后生成的数不加括号
	        else if (i != operator_no + 1) {
	            // 判断是否生成括号
	            bracket = rand.nextInt(3);
	            // backet=0时,不生成括号
	            if (bracket == 0) {
	                ac = ac + createNum() + operator[rand.nextInt(4)];
	            }
	            // bracket=1时，在表达式前方生成括号,bracket_no+1
	            else if (bracket == 1) {
	                ac = "(" + ac + createNum() +operator[rand.nextInt(4)];
	                bracket_no++;
	            }
	            // bracket=2时，在表达式后后方加括号,bracket_no+1
	            else {
	                ac = ac + "(" + createNum() + operator[rand.nextInt(4)];
	                bracket_no++;
	            }
	        } else {
	            ac = ac + createNum() + operator[rand.nextInt(4)];
	        }
	    }
	    // 是否存在未配对的"(",存在就补上
	    if (bracket_no != 0) {
	        ac = ac.substring(0, ac.length() - 1);
	        for (int i = 0; i < bracket_no; bracket_no--) {
	            ac = ac + ")";
	        }
	    }
	    // 去除多余的运算符
	    else {
	        ac = ac.substring(0, ac.length() - 1);
	    }
	 
	    return ac;
	}
	public static String createNum() {
		String num="";
		Random rand = new Random();
		int flag = rand.nextInt(2);
		if(flag==0){
			num=String.valueOf(rand.nextInt(30)+1);
	    }
		else{
		    num=createFraction();
		}
		return num;
	}
	public static String createFraction(){  //随机组成真分母           博客
		Random rand = new Random();
        int[] fraction=new int[2];
        int fraction1 = rand.nextInt(8)+1;//避免分子出现零
        int fraction2 = rand.nextInt(8)+1;//避免分母出现零
        if(fraction1==fraction2){            //避免出现分子分母相同
        	return createFraction();
        }else{
        	 if(fraction1<fraction2){        //避免出现假分数
             	fraction[0]=fraction1;
             	fraction[1]=fraction2;
             	
             }else{
             	fraction[0]=fraction2;
             	fraction[1]=fraction1;  
             }
        	 return fraction[0]+"/"+fraction[1];
        }   
    }
	 private static String toStringMethod(int[] arr)   
	    {  
	        // 自定义一个字符缓冲区，  
	        StringBuilder sb = new StringBuilder();  
	        sb.append("[ ");  
	        //遍历int数组，并将int数组中的元素转换成字符串储存到字符缓冲区中去  
	        for(int i=0;i<arr.length;i++)  
	        {  
	            if(i!=arr.length-1)  
	                sb.append(arr[i]+" ,");  
	            else  
	                sb.append(arr[i]+" ]");   
	        }  
	        return sb.toString();
	    }
	
	public mainF()//计算四则运算并输出
	{
		new Thread(myTimeRunable).start();
		int n = 0;
		MainFrame ma= new MainFrame();
		try{
			//调用初始化方法
			jbInit();
		}
		catch(Exception exception){
			exception.printStackTrace();
		}
	
		try{
			
			n= Integer.parseInt(ma.number);
			}catch(NumberFormatException e){
				//利用消息对话框提示输入失败
				JOptionPane.showMessageDialog(this,"輸入錯誤！請重新輸入");
				}
		for(int i=0;i<n;i++){
			String que=createAc(3);
			Question.add(que);
            CalcFunction calc = new CalcFunction();
            Answer.add(calc.ca(que)+"");
        }
        //输出题目
        JTextArea0.setText("");
        for(String string : Question){
            num ++;
            JTextArea0.append("("+num+")、"+string+"\n");
        }
	}

	//计时器
	 private class MyRunable implements Runnable{
	        private int hour = 0;
	        private int min = 0;
	        private int sec = 0;
	        private NumberFormat format = NumberFormat.getInstance();
	        private String nextSecond(){
	            ++sec;
	            if(sec == 60) {
	                ++min;
	                sec = 0;
	            }
	             
	            if(min == 60) {
	                ++hour;
	                min = 0;
	            }
	            return currentTime();
	        }
	         
	        private String currentTime(){
	            return format.format(hour)+":"+format.format(min)+":"+format.format(sec);
	        }
	         
	        @Override
	        public void run() {
	            format.setMinimumIntegerDigits(2);
	            format.setGroupingUsed(false);
	            while(true) {
					if(rootPaneCheckingEnabled) {
	                    if(isRun) {
	                        nextSecond();
	                        jLabel4.setText(currentTime());
	                    }
	                }
	                try {
	                    Thread.sleep(1000);
	                }catch (InterruptedException e) {
	                }
	            }
	        }
	         
	    }
	@Override
	//菜单项事件对应的处理方法
	public void actionPerformed(ActionEvent e) {
				 // TODO Auto-generated method stub
		        //点击“系统”菜单下的“退出”菜单项
				if(e.getSource()==jButton0)
				{
					int num1=0;
					JTextArea2.setText("");
			        for(String string : Answer){//输出正确答案
			            num1 ++;
			            JTextArea2.append("("+num1+")、"+string+"\n");
			        }
			        
			        //获取答案并对比得正确率
			             String[] ans =  JTextArea1.getText().split("\n");
			          		for(int i = 0;i < ans.length;i++){
			          			answer.add(ans[i]+"");
			          		}
			                 for(String str:answer)  
			                 {  
			                     if(Answer.contains(str))  
			                     {  
			                        right++;
			                     }  
			                 }
			      		
			        int sum;
		     		sum=right*100/num1;
		     		JTextArea3.append(sum+"%");
					isRun = false;
				}
				//计时开始
				if(e.getSource()==jButton2)
				{
					 isRun = true;
				}
				//点击下一题
				if(e.getSource()==jButton1)
				{
					MainFrameF MFF=new MainFrameF();
					//移除主框架上原有的内容
					this.remove(this.getRootPane());
					this.dispose();
				    MFF.setVisible(true);
				}
	}
	}