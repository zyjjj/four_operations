package Calcular;

//系统主框架类

//导入系统的包
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
//创建主框架类MainFrame
public class MainFrame extends JFrame implements ActionListener{
	public static String number;
	private MainFrame dl;
	
		//创建内容面板
	JPanel contentPane;
	//创建菜单栏组件的对象
	JMenuBar jMenuBar1=new JMenuBar();//菜单条
	JMenu jMenu0=new JMenu("系统");//菜单
	JMenuItem jMenuItem0=new JMenuItem("退出");//子菜单
	JMenu jMenu1=new JMenu("文字转换");
	JMenuItem jMenuItem1=new JMenuItem("简体中文");
	JMenuItem jMenuItem2=new JMenuItem("繁體中文");
	JMenuItem jMenuItem3=new JMenuItem("English");
	JMenu jMenu2=new JMenu("帮助");
	JMenuItem jMenuItem4=new JMenuItem("使用说明");
	JLabel jLabel1=new JLabel("请输入题目数量：");
	JTextField jTextField1=new JTextField();
	JButton jButton1=new JButton("开始做题");
	//创建构造方法
	public MainFrame()
	{
		try{
			//关闭框架窗口时的默认事件方法
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			//调用初始化方法
			jbInit();
		}
		catch(Exception exception){
			exception.printStackTrace();
		}
	}
	//界面初始化方法
	private void jbInit() throws Exception
	{
		//创建内容面板
		contentPane =(JPanel) getContentPane();
		//设置内容面板的布局为空
		contentPane.setLayout(null);
		//主框架的大小
		setSize(new Dimension(500,450));
		//主框架的标题
		setTitle("四则运算系统");
		//添加事件监听器
		jMenuItem0.addActionListener(this);
		jMenuItem1.addActionListener(this);
		jMenuItem2.addActionListener(this);
		jMenuItem3.addActionListener(this);
		jMenuItem4.addActionListener(this);
		jButton1.addActionListener(this);
		//添加菜单条到主框架
		setJMenuBar(jMenuBar1);
		//添加菜单到菜单条
		jMenuBar1.add(jMenu0);
		jMenuBar1.add(jMenu1);
		jMenuBar1.add(jMenu2);
		//添加菜单项到菜单
		jMenu0.add(jMenuItem0);
		jMenu1.add(jMenuItem1);
		jMenu1.add(jMenuItem2);
		jMenu1.add(jMenuItem3);
		jMenu2.add(jMenuItem4);
		//将组件加入面板
		this.add(jLabel1);
		this.add(jTextField1);
		this.add(jButton1);
		//设置各组件大小
		jLabel1.setFont(new java.awt.Font("楷体",Font.BOLD,20));
		jLabel1.setBounds(new Rectangle(40,89,170,30));
		jTextField1.setBounds(new Rectangle(225,90,200,30));
		jButton1.setBounds(new Rectangle(180,200,120,30));
	}
	//事件对应的处理方法
	public void actionPerformed(ActionEvent e)
	{
		//点击“系统”菜单下的“退出”菜单项
		if(e.getSource()==jMenuItem0)
		{
			System.exit(0);
		}
		//点击文字转换下的繁体中文
		if(e.getSource()==jMenuItem2)
		{
			//创建繁体中文的面板对象
			MainFrameF MFF=new MainFrameF();
			this.remove(this.getRootPane());
			this.dispose();
		    MFF.setVisible(true);

		}
		//点击文字转换下的英文
		if(e.getSource()==jMenuItem3)
		{
			//创建英文的面板对象
			MainFrameE MFE=new MainFrameE();
			this.remove(this.getRootPane());
			this.dispose();
		    MFE.setVisible(true);

		}
		//点击开始做题
		if(e.getSource()==jButton1)
		{
			number=jTextField1.getText();
			//创建四则运算的面板对象
			main M=new main();
			//移除主框架上原有的内容
			this.remove(this.getContentPane());
			this.dispose();
			//令界面可见
			M.setVisible(true);

		}
	
	}
}
	