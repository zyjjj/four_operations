/**
 * 
 */
package Calcular;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CalcFunction {
	    private String result;
	    public String ca(String s){
	    	String a,na=null;
	    	while(s.indexOf("(")>-1){
	    		int inner = s.lastIndexOf("(");
		    	a = s.substring(inner+1, s.indexOf(")",inner));
		    	s=s.replace("("+a+")", cal(a));
		    }
		    return cal(s);
	    }
	    public int minIndex(String s,int i){                      //乘或除计算后位数的位置
	    	int min=0;
	    	if(s.charAt(i)==45) i=i+1;
	    	List<Integer> index = new ArrayList<Integer>();
	    	if(s.indexOf("+", i)>0) index.add(s.indexOf("+", i));
	    	if(s.indexOf("-", i)>0) index.add(s.indexOf("-", i));
	    	if(s.indexOf("*", i)>0) index.add(s.indexOf("*", i));
	    	if(s.indexOf("÷", i)>0) index.add(s.indexOf("÷", i));
	        if(index.isEmpty()) min=s.length()-1;
	        else min= Collections.min(index)-1;
	    	return min;
	    }
	    public String cal(String s){          
	    	while(s.contains("*")||s.contains("÷")){           //替换乘除运算
    			int b,x=0;
    			if(s.indexOf("*")<0) b=s.indexOf("÷");
    			else if(s.indexOf("÷")<0) b=s.indexOf("*");
    			else b=s.indexOf("*")>s.indexOf("÷")?s.indexOf("÷"):s.indexOf("*");
    			if(s.substring(0,b).contains("+")||s.substring(0,b).contains("-")){
    				x=(s.lastIndexOf("+",b)>s.lastIndexOf("-", b)?s.lastIndexOf("+",b):s.lastIndexOf("-", b))+1;  //上一位
    			}
    			int y=minIndex(s,b+1);                          //下一位
    			s=s.replace(s.substring(x,y+1), calc(s.substring(x,y+1)));
    		}
	    	if(s.contains("+-")) s=s.replaceAll("[+][-]", "-");
	    	if(s.contains("--")) s=s.replaceAll("--", "+");
	    	while(!(s.startsWith("-") &&s.substring(1).indexOf("+")==-1&&s.substring(1).indexOf("-")==-1)){
		    	            //计算单纯加减法
		    	String p=null;
		    	String[] authors = s.split("[+]");
		    	if(s.indexOf("+")<-1) authors[0]=s;
			    for (int i = 0; i < authors.length; i++) {
			    	int a=0;
			        if(authors[i].contains("-")){
			        	if(authors[i].startsWith("-")) {a=1;authors[i]=authors[i].substring(1);}
			        	String[] newa = authors[i].split("-");
			        	
			        	if(a==1) s="-"+calc(newa[0]+"+"+newa[1]);       //-1-2情况
			        	else s=calc(newa[0]+"-"+newa[1]);
			    		for (int j = 2; j < newa.length; j++) {
			    			if(s.contains("-")) s="-"+calc(s+"+"+newa[j]);               //被减数为负数
			    			else s=calc(s+"-"+newa[j]);
			    		}
			    		//if(s.contains("-")) {t=1;p=s;s="0";}
			    		authors[i]=s;
			        }
			    }
			    if(authors.length>1){                     //计算单纯加法
		        	for (int i = 1; i < authors.length; i++) {
		        		if(authors[i].contains("-")&&!authors[0].contains("-")) authors[0]=calc(authors[0]+"-"+authors[i].substring(1));
		        		else if(authors[0].contains("-")&&!authors[1].contains("-")) authors[0]=calc(authors[1]+"-"+authors[0].substring(1));
		        		else authors[0]=calc(authors[0]+"+"+authors[i]);
		    		}
		        		s=authors[0];
			    }
			    else {s=authors[0];
			    break;}
	    	}
		    return s;
	    }
	    
	    public String calc(String s){
	    	if(s.indexOf("*")>-1){
	        	int i=s.indexOf("*");            
	            if(s.indexOf("*",i+1)==i+1){           
	            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
	               }else{
	            	   multiply(s);
	               }     
	    	}
	    	else if(s.indexOf("÷")>-1){
		        	int i=s.indexOf("÷"); 
		            if(s.indexOf("÷",i+1)==i+1){           
		            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
		               }else{
		            	   divide(s);
		               } 
	        }
	    	else if(s.indexOf("+")>-1){
	    		int i=s.indexOf("+");
	    		if(s.indexOf("+",i+1)==i+1){
	    			throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
	    			}else{
	    				add(s);
	    				}
	    		}
	    	else if(s.indexOf("-")>-1){
		        	int i=s.indexOf("-");            
		            if(s.indexOf("-",i+1)==i+1){           
		            	throw new IllegalArgumentException("Input error! Don't like 1++1");//格式错误时抛出异常
		               }else{
		            	   substract(s);
		               }
		        
	        }
	        return getResult();
	    }
	    
	    public void add(String s)//加法
	    {	
	    	String t="";
	        String[] str=s.split("[+]");
	        if(str[0].contains("-")&&str[1].contains("-")){
	        	str[0]=str[0].substring(1);str[1]=str[1].substring(1);
	        	t="-";
	        }
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=new String[2],str2=new String[2];
	        	if(!str[0].contains("/")) {str1[0]=String.valueOf(Integer.parseInt(str[0])*Integer.parseInt(str[0]));str1[1]=str[0];}
        	 	else  {str1=str[0].split("[/]");}
        	if(!str[1].contains("/")) {str2[0]=String.valueOf(Integer.parseInt(str[1])*Integer.parseInt(str[1]));str2[1]=str[1];}
        		else  {str2=str[1].split("[/]");}
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	        		result =t+simplefraction(((Integer.parseInt(str1[0])*Integer.parseInt(str2[1]))+(Integer.parseInt(str2[0])*Integer.parseInt(str1[1]))),(Integer.parseInt(str1[1])*Integer.parseInt(str2[1])));      	
	        	}else{
	        		if(Integer.parseInt(str1[0])== 0) result= t+s.substring(2);
	        		else if(Integer.parseInt(str2[0])== 0) result= t+s.substring(0,s.length()-2);
	        		else throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	        }
	        else{//整数
	 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
	            {	           
	 	    	   result = Integer.parseInt(str[0])+Integer.parseInt(str[1])+""; 
	 	    	   }       	
	        
	       else{
	    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	       } 
	        
	    }
	    
	    public void substract(String s)//减法
	    {
	        String[] str=s.split("[-]");
	        if(str.length==1) {result=s;}
	        else if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=new String[2],str2=new String[2];
	        	if(!str[0].contains("/")) {str1[0]=String.valueOf(Integer.parseInt(str[0])*Integer.parseInt(str[0]));str1[1]=str[0];}
	        	 	else  {str1=str[0].split("[/]");}
	        	if(!str[1].contains("/")) {str2[0]=String.valueOf(Integer.parseInt(str[1])*Integer.parseInt(str[1]));str2[1]=str[1];}
	        		else  {str2=str[1].split("[/]");}
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	        		result =simplefraction(((Integer.parseInt(str1[0])*Integer.parseInt(str2[1]))-(Integer.parseInt(str2[0])*Integer.parseInt(str1[1]))),(Integer.parseInt(str1[1])*Integer.parseInt(str2[1])));    	
	        	}else{
	        		if(Integer.parseInt(str1[0])== 0) result= s.substring(1);
	        		else if(Integer.parseInt(str2[0])== 0) result= s.substring(0,s.length()-2);
	        		else throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	        }
	        else{//整数
	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
	            {
	    	   result = Integer.parseInt(str[0])-Integer.parseInt(str[1])+"";
	    	   }       	
	        
	       else{
	    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	       }
	    }
	    
	    
	    public void multiply(String s)//乘法
	    {
	    	int i=0;
	    	if(s.startsWith("-")) {s=s.substring(1);i=i+1;}
	    	if(s.contains("*-")) {s=s.replace("*-", "*");i=i+1;}
	    	String a="";
	    	if(i%2==1)
	        	a="-";
	        String[] str=s.split("[*]");
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=new String[2],str2=new String[2];
	        	if(!str[0].contains("/")) {str1[0]=String.valueOf(Integer.parseInt(str[0])*Integer.parseInt(str[0]));str1[1]=str[0];}
        	 	else  {str1=str[0].split("[/]");}
	        	if(!str[1].contains("/")) {str2[0]=String.valueOf(Integer.parseInt(str[1])*Integer.parseInt(str[1]));str2[1]=str[1];}
        		else  {str2=str[1].split("[/]");}
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)
	        	{
	            	result =a+simplefraction(Integer.parseInt(str1[0])*Integer.parseInt(str2[0]),Integer.parseInt(str1[1])*Integer.parseInt(str2[1]));    	
	            	
	        	}else{
	        		if(Integer.parseInt(str1[0])== 0 || Integer.parseInt(str2[0])== 0) result= "0";
	        		else throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	        }
	        else{//整数
		 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
		            {	     
		 	    	   result = a+Integer.parseInt(str[0])*Integer.parseInt(str[1])+"";   
		            }
		        
		       else{
		    	   throw new IllegalArgumentException("overrun!");}//数值范围超出时抛出异常
	        	
	        	}
	    }
	    
	    
	    public void divide(String s)//除法
	    {
	    	int i=0;
	    	if(s.contains("÷-")) {s=s.replace("÷-", "÷");i=i+1;}
	    	if(s.startsWith("-")) {s=s.substring(1);i=i+1;}
	        String[] str=s.split("[÷]");
	        
	        if(str[0].indexOf("/")>-1 || str[1].indexOf("/")>-1)//分数
	        {
	        	String[] str1=new String[2],str2=new String[2];
	        	if(!str[0].contains("/")) {str1[0]=String.valueOf(Integer.parseInt(str[0])*Integer.parseInt(str[0]));str1[1]=str[0];}
        	 	else  {str1=str[0].split("[/]");}
	        	if(!str[1].contains("/")) {str2[0]=String.valueOf(Integer.parseInt(str[1])*Integer.parseInt(str[1]));str2[1]=str[1];}
        		else  {str2=str[1].split("[/]");}
	        	if(Integer.parseInt(str1[1]) != 0 && Integer.parseInt(str2[1]) != 0)//分母不为零
	        	{
	            	result =simplefraction(Integer.parseInt(str1[0])*Integer.parseInt(str2[1]),Integer.parseInt(str1[1])*Integer.parseInt(str2[0]));  	
	        	}else{
	        		if(Integer.parseInt(str1[0])== 0 || Integer.parseInt(str2[0])== 0) result= "0";
	        		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
	        	}
	  
	        }else{
	        	if(Integer.parseInt(str[1]) != 0){//整数
			 	       if( Integer.parseInt(str[0])<1000&&Integer.parseInt(str[1])<1000&&Integer.parseInt(str[0])>-1000&&Integer.parseInt(str[1])>-1000)
			            {	            
			 	    	   result = Integer.parseInt(str[0])/Integer.parseInt(str[1])+"";
			            }       	
			        
			       else{
			    	   throw new IllegalArgumentException("overrun!");}
	                         		
	        	}else {
	        		
	        		throw new IllegalArgumentException("Divisor cannot be zero!");   //除数为零时抛出异常  	
	        	}
	        }
	        if(i%2==1)
	        	result="-"+result;
	    }
    
    public String getResult()
    {
        return result;
    } 
    public static int GCD(int m, int n) {//递归法求最大公约数
	    if(m<0) m=-m;
	    if(n<0) n=-n;
        if(m % n == 0){
            return n;
        }else{
            while(m != n){
                if(m > n)m = m - n;
                else n = n - m;
            }
        }
        return m;
    }
    public static int LCM(int m, int n){//求最小公倍数
        return m * n / GCD(m,n);
    }
    
    static String simplefraction(int m, int n){ //化简分数
	    String s;
	    int num = GCD(m, n);
	            m = m / num;
	            n = n / num;
	    if (n == 1) {
	        s = m + "";
	    } else {
	        s = m + "" + "/" + n + "";
	    } 
	    return s;
    }
}